<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>com.egf</groupId>
	<artifactId>core</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<name>core</name>

	<properties>
		<!-- client -->
		<gwt.version>2.7.0</gwt.version>
		<gwtp.version>1.5.2</gwtp.version>
		<gin.version>2.1.2</gin.version>

		<!-- server -->
		<guice.version>4.0</guice.version>

		<!-- testing -->
		<junit.version>4.12</junit.version>
		<jukito.version>1.4.1</jukito.version>

		<!-- maven -->
		<gwt-maven-plugin.version>2.7.0</gwt-maven-plugin.version>
		<maven-surefire-plugin.version>2.17</maven-surefire-plugin.version>
		<maven-compiler-plugin.version>3.2</maven-compiler-plugin.version>
		<maven-war-plugin.version>2.5</maven-war-plugin.version>
		<maven-resources-plugin.version>2.5</maven-resources-plugin.version>
		<maven-processor-plugin.version>2.0.5</maven-processor-plugin.version>
		<maven-build-helper-plugin.version>1.7</maven-build-helper-plugin.version>

		<target.jdk>1.7</target.jdk>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

		<webappDirectory>${project.build.directory}/${project.build.finalName}</webappDirectory>

	</properties>

	<pluginRepositories>
		<pluginRepository>
			<id>synergian-repo</id>
			<url>https://raw.github.com/synergian/wagon-git/releases</url>
		</pluginRepository>
	</pluginRepositories>

	<distributionManagement>
		<repository>
			<id>com.egf.core.releases</id>
			<name>Core Releases</name>
			<url>git:releases://git@bitbucket.org:deineka/maven.git</url>
		</repository>
		<snapshotRepository>
			<id>com.egf.core.snapshots</id>
			<name>Core Snapshots</name>
			<url>git:snapshots://git@bitbucket.org:deineka/maven.git</url>
		</snapshotRepository>
		<!-- <site> <id>com.egf.core.site</id> <url>git:site://git@bitbucket.org:deineka/core.git</url> </site> -->
	</distributionManagement>

	<build>
		<extensions>
			<extension>
				<groupId>ar.com.synergian</groupId>
				<artifactId>wagon-git</artifactId>
				<version>0.2.5</version>
			</extension>
		</extensions>

		<plugins>
			<plugin>
				<artifactId>maven-resources-plugin</artifactId>
				<version>${maven-resources-plugin.version}</version>
				<executions>
					<execution>
						<id>copy-resources</id>
						<phase>process-classes</phase>
						<goals>
							<goal>copy-resources</goal>
						</goals>
						<configuration>
							<outputDirectory>${basedir}/target/classes</outputDirectory>
							<encoding>UTF-8</encoding>
							<resources>
								<resource>
									<directory>${basedir}/src/main/java</directory>
									<includes>
										<filtering>false</filtering>
										<include>**/*.xml</include>
										<include>**/*.properties</include>
										<include>**/client/**/*.java</include>
										<include>**/shared/**/*.java</include>
									</includes>
								</resource>
							</resources>
						</configuration>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>

	<repositories>
		<repository>
			<id>google-api-services</id>
			<url>https://oss.sonatype.org/content/repositories/releases/</url>
		</repository>
		<repository>
			<id>google-api-services-beta</id>
			<url>http://google-api-client-libraries.appspot.com/mavenrepo</url>
		</repository>
	</repositories>

	<dependencies>

		<!-- JPA -->
		<dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-entitymanager</artifactId>
			<version>4.2.0.Final</version>
		</dependency>
		<dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-validator</artifactId>
			<version>4.2.0.Final</version>
		</dependency>
		<dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-validator</artifactId>
			<version>4.2.0.Final</version>
			<classifier>sources</classifier>
		</dependency>
		<dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-c3p0</artifactId>
			<version>4.2.0.Final</version>
		</dependency>
		<dependency>
			<groupId>kr.pe.kwonnam.hibernate4memcached</groupId>
			<artifactId>hibernate4-memcached-core</artifactId>
			<version>0.7</version>
		</dependency>
		<dependency>
			<groupId>kr.pe.kwonnam.hibernate4memcached</groupId>
			<artifactId>hibernate4-memcached-spymemcached-adapter</artifactId>
			<version>0.7</version>
		</dependency>
		<!-- <dependency> <groupId>org.jboss.jbossts</groupId> <artifactId>jbossjta</artifactId> <version>4.16.6.Final</version> </dependency> <dependency> <groupId>org.jboss.jbossts</groupId> <artifactId>common</artifactId> <version>4.17.29.Final</version> </dependency> -->

		<!-- Guava -->
		<dependency>
			<groupId>com.google.guava</groupId>
			<artifactId>guava-gwt</artifactId>
			<version>18.0</version>
		</dependency>

		<!-- Commons -->
		<dependency>
			<groupId>commons-fileupload</groupId>
			<artifactId>commons-fileupload</artifactId>
			<version>1.3.1</version>
		</dependency>
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-email</artifactId>
			<version>1.3.3</version>
		</dependency>
		<dependency>
			<groupId>commons-lang</groupId>
			<artifactId>commons-lang</artifactId>
			<version>2.6</version>
		</dependency>
		<dependency>
			<groupId>org.apache.httpcomponents</groupId>
			<artifactId>fluent-hc</artifactId>
			<version>4.5</version>
		</dependency>
		<dependency>
			<groupId>org.apache.httpcomponents</groupId>
			<artifactId>httpmime</artifactId>
			<version>4.5</version>
		</dependency>
		<dependency>
			<groupId>commons-codec</groupId>
			<artifactId>commons-codec</artifactId>
			<version>1.10</version>
		</dependency>


		<!-- Google Web Toolkit -->
		<dependency>
			<groupId>com.google.gwt</groupId>
			<artifactId>gwt-dev</artifactId>
			<version>${gwt.version}</version>
		</dependency>
		<dependency>
			<groupId>com.google.gwt</groupId>
			<artifactId>gwt-user</artifactId>
			<version>${gwt.version}</version>
		</dependency>
		<!-- <dependency> <groupId>com.google.apis</groupId> <artifactId>google-api-services-customsearch</artifactId> <version>v1-rev46-1.20.0</version> <exclusions> <exclusion> <artifactId>guava-jdk5</artifactId> <groupId>com.google.guava</groupId> </exclusion> <exclusion> <artifactId>httpclient</artifactId> 
			<groupId>org.apache.httpcomponents</groupId> </exclusion> </exclusions> </dependency> -->
		<dependency>
			<groupId>org.vectomatic</groupId>
			<artifactId>lib-gwt-file</artifactId>
			<version>0.3.6</version>
		</dependency>
		<dependency>
			<groupId>com.googlecode.gwt-charts</groupId>
			<artifactId>gwt-charts</artifactId>
			<version>0.9.10</version>
		</dependency>

		<!-- GWT-Platform -->
		<dependency>
			<groupId>com.gwtplatform</groupId>
			<artifactId>gwtp-mvp-client</artifactId>
			<version>${gwtp.version}</version>
		</dependency>
		<dependency>
			<groupId>com.gwtplatform</groupId>
			<artifactId>gwtp-dispatch-rpc-client</artifactId>
			<version>${gwtp.version}</version>
		</dependency>
		<dependency>
			<groupId>com.gwtplatform</groupId>
			<artifactId>gwtp-dispatch-rpc-server-guice</artifactId>
			<version>${gwtp.version}</version>
		</dependency>
		<dependency>
			<groupId>com.gwtplatform</groupId>
			<artifactId>gwtp-dispatch-rpc-shared</artifactId>
			<version>${gwtp.version}</version>
		</dependency>
		<dependency>
			<groupId>com.gwtplatform</groupId>
			<artifactId>gwtp-processors</artifactId>
			<version>${gwtp.version}</version>
		</dependency>

		<!-- DI -->
		<dependency>
			<groupId>com.google.inject</groupId>
			<artifactId>guice</artifactId>
			<version>${guice.version}</version>
		</dependency>
		<dependency>
			<groupId>com.google.inject.extensions</groupId>
			<artifactId>guice-persist</artifactId>
			<version>${guice.version}</version>
		</dependency>
		<dependency>
			<groupId>com.google.inject.extensions</groupId>
			<artifactId>guice-servlet</artifactId>
			<version>${guice.version}</version>
		</dependency>
		<dependency>
			<groupId>com.google.inject.extensions</groupId>
			<artifactId>guice-assistedinject</artifactId>
			<version>${guice.version}</version>
		</dependency>
		<dependency>
			<groupId>com.google.gwt.inject</groupId>
			<artifactId>gin</artifactId>
			<version>${gin.version}</version>
		</dependency>


		<!-- Bootstrap -->
		<dependency>
			<groupId>org.gwtbootstrap3</groupId>
			<artifactId>gwtbootstrap3</artifactId>
			<version>0.9.3</version>
		</dependency>
		<dependency>
			<groupId>org.gwtbootstrap3</groupId>
			<artifactId>gwtbootstrap3-extras</artifactId>
			<version>0.9.3</version>
		</dependency>

		<!-- Social -->
		<dependency>
			<groupId>com.google.api</groupId>
			<artifactId>gwt-oauth2</artifactId>
			<version>0.2</version>
			<exclusions>
				<exclusion>
					<artifactId>gwt-user</artifactId>
					<groupId>com.google.gwt</groupId>
				</exclusion>
				<exclusion>
					<artifactId>gwt-dev</artifactId>
					<groupId>com.google.gwt</groupId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>com.flickr4java</groupId>
			<artifactId>flickr4java</artifactId>
			<version>2.12</version>
		</dependency>

		<!-- Other -->
		<dependency>
			<groupId>net.sourceforge.jexcelapi</groupId>
			<artifactId>jxl</artifactId>
			<version>2.6.12</version>
		</dependency>
		<dependency>
			<groupId>org.quartz-scheduler</groupId>
			<artifactId>quartz</artifactId>
			<version>2.2.1</version>
		</dependency>
		<dependency>
			<groupId>com.google.code.gson</groupId>
			<artifactId>gson</artifactId>
			<version>2.3.1</version>
		</dependency>
		<dependency>
			<groupId>org.postgresql</groupId>
			<artifactId>postgresql</artifactId>
			<version>9.4-1201-jdbc41</version>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-log4j12</artifactId>
			<version>1.7.12</version>
		</dependency>
		<dependency>
			<groupId>org.mortbay.jetty</groupId>
			<artifactId>jetty</artifactId>
			<version>6.1.26</version>
		</dependency>
		<dependency>
			<groupId>com.twelvemonkeys.imageio</groupId>
			<artifactId>imageio-jpeg</artifactId>
			<version>3.1.0</version>
		</dependency>


		<!-- MAPS -->
		<dependency>
			<groupId>com.github.branflake2267</groupId>
			<artifactId>gwt-maps-api</artifactId>
			<version>3.10.0-alpha-7</version>
		</dependency>

		<!-- JSON -->
		<dependency>
			<groupId>javax.json</groupId>
			<artifactId>javax.json-api</artifactId>
			<version>1.0</version>
		</dependency>
		<dependency>
			<groupId>org.glassfish</groupId>
			<artifactId>javax.json</artifactId>
			<version>1.0.4</version>
		</dependency>

		<dependency>
			<groupId>org.reflections</groupId>
			<artifactId>reflections</artifactId>
			<version>0.9.9-RC1</version>
		</dependency>

	</dependencies>

</project>